package mpetrychko.revoluttestapp.data.remote

import com.google.common.truth.Truth.assertThat
import com.nhaarman.mockito_kotlin.*
import io.reactivex.Single
import mpetrychko.revoluttestapp.domain.Currency
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class CurrencyNetworkDataSourceTest {

    @Mock
    private lateinit var ratesApi: RatesApi

    private lateinit var currencyNetworkDataSource: CurrencyNetworkDataSource

    private lateinit var closeable: AutoCloseable

    private val testCurrency = Currency("c", 1.0, true)
    private val ratesResponse = RatesResponse(
        testCurrency.currencyName.orEmpty(),
        hashMapOf("c1" to 10.0, "c2" to 1.0, "c3" to 3.0, "c4" to 8.0)
    )

    @Before
    fun init() {
        closeable = MockitoAnnotations.openMocks(this)
        whenever(ratesApi.loadRates(testCurrency.currencyName.orEmpty())) doReturn Single.just(
            ratesResponse
        )

        currencyNetworkDataSource = CurrencyNetworkDataSource(ratesApi)
    }

    @After
    @Throws(Exception::class)
    fun tearDown() {
        closeable.close()
    }

    @Test
    fun whenLoadRates_theyAreCorrectlyMapped() {
        val singleResult =
            currencyNetworkDataSource.loadCurrencyRelativeRate(testCurrency)

        verify(ratesApi, times(1)).loadRates(eq(testCurrency.currencyName.orEmpty()))

        val testObserver = singleResult.test()
        testObserver.assertValueCount(1)

        val resultValue = testObserver.values().first()
        assertThat(resultValue).isNotNull()
        assertThat(resultValue.baseCurrency).isEqualTo(testCurrency)
        assertThat(resultValue.targetCurrencies.size).isEqualTo(ratesResponse.rates.size)
        ratesResponse.rates.forEach { e ->
            val mappedCurrency = resultValue.targetCurrencies.find { it.currencyName == e.key }
            assertThat(mappedCurrency).isNotNull()
            assertThat(mappedCurrency?.currencyName).isEqualTo(e.key)
            assertThat(mappedCurrency?.currencyRate).isEqualTo(e.value)
            assertThat(mappedCurrency?.isBaseCurrency).isFalse()
        }
    }
}