package mpetrychko.revoluttestapp.data.local

import com.google.common.truth.Truth
import org.junit.Test

class CurrencyMapperTest {

    private val currencyMapper = CurrencyMapper()

    @Test
    fun mapCurrencyEntityReturnsCorrectValues() {
        val currencyEntity = CurrencyEntity("a", "p", 2.333, isBaseCurrency = false)
        val currency = currencyMapper(currencyEntity)
        Truth.assertThat(currency.currencyName).isEqualTo(currencyEntity.currencyName)
        Truth.assertThat(currency.currencyRate).isEqualTo(currencyEntity.rate)
        Truth.assertThat(currency.isBaseCurrency).isEqualTo(currencyEntity.isBaseCurrency)
    }
}