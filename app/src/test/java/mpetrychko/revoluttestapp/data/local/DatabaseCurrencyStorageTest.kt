package mpetrychko.revoluttestapp.data.local

import com.google.common.truth.Truth.assertThat
import com.nhaarman.mockito_kotlin.*
import mpetrychko.revoluttestapp.MockitoHelper
import mpetrychko.revoluttestapp.domain.Currency
import mpetrychko.revoluttestapp.domain.RelativeCurrency
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentCaptor
import org.mockito.Captor
import org.mockito.Mock
import org.mockito.MockitoAnnotations


class DatabaseCurrencyStorageTest {

    @Mock
    private lateinit var currencyDao: CurrencyDao

    @Mock
    private lateinit var currencyMapper: (CurrencyEntity) -> Currency

    @Mock
    private lateinit var currencyEntityMapper: (Currency, String?) -> CurrencyEntity

    @Captor
    private lateinit var currencyEntitiesCaptor: ArgumentCaptor<List<CurrencyEntity>>

    private val testCurrency = Currency("c", 1.0, false)
    private val testRelativeCurrency = RelativeCurrency(
        Currency("c1", 1.0, true),
        arrayListOf(
            Currency("c2", 1.0, false),
            Currency("c3", 1.0, false),
            Currency("c4", 1.0, false)
        )
    )
    private val testBaseCurrencyEntity = CurrencyEntity("c1", "p1", 1.0, true)
    private val testCurrencyEntities = arrayListOf(
        CurrencyEntity("c1", "p1", 1.0, false),
        CurrencyEntity("c2", "p2", 2.0, false),
        CurrencyEntity("c3", "p3", 3.0, false),
        CurrencyEntity("c4", "p4", 4.0, false)
    )

    private lateinit var currencyStorage: DatabaseCurrencyStorage

    private lateinit var closeable: AutoCloseable

    @Before
    fun init() {
        closeable = MockitoAnnotations.openMocks(this)

        currencyStorage =
            DatabaseCurrencyStorage(currencyDao, currencyEntityMapper, currencyMapper)
        whenever(currencyDao.loadRelativeCurrency(any())) doReturn testCurrencyEntities
        whenever(currencyDao.findBaseCurrency()) doReturn testBaseCurrencyEntity
        whenever(currencyMapper.invoke(any())) doReturn testCurrency
    }

    @After
    @Throws(Exception::class)
    fun tearDown() {
        closeable.close()
    }

    @Test
    fun whenLoadRelativeCurrency_returnsRelativeCurrency() {
        val relativeCurrency = currencyStorage.loadRelativeCurrency(testCurrency)
        assertThat(relativeCurrency).isNotNull()
        assertThat(relativeCurrency?.baseCurrency).isEqualTo(testCurrency)
        assertThat(relativeCurrency?.targetCurrencies?.size)
            .isEqualTo(testCurrencyEntities.size)

        testCurrencyEntities.forEach { verify(currencyMapper, times(1)).invoke(it) }
    }

    @Test
    fun whenNoRelativeCurrency_returnsNull() {
        whenever(currencyDao.loadRelativeCurrency(any())) doReturn null

        val relativeCurrency = currencyStorage.loadRelativeCurrency(testCurrency)
        assertThat(relativeCurrency).isNull()
    }

    @Test
    fun whenSaveRelativeCurrency_itIsSaved() {
        currencyStorage.saveRelativeCurrency(testRelativeCurrency)

        verify(currencyEntityMapper, times(1)).invoke(testRelativeCurrency.baseCurrency, null)
        testRelativeCurrency.targetCurrencies.forEach {
            verify(
                currencyEntityMapper,
                times(1)
            ).invoke(it, testRelativeCurrency.baseCurrency.currencyName)
        }

        verify(currencyDao, times(1)).insertAll(MockitoHelper.capture(currencyEntitiesCaptor))
        val capturedEntities = currencyEntitiesCaptor.value
        assertThat(capturedEntities).isNotNull()
        assertThat(capturedEntities).isNotEmpty()
        assertThat(capturedEntities.size).isEqualTo(testRelativeCurrency.targetCurrencies.size + 1)
    }

    @Test
    fun whenLoadBaseCurrency_itIsReturned() {
        val baseCurrency = currencyStorage.loadBaseCurrency()
        assertThat(baseCurrency).isNotNull()
        verify(currencyDao, times(1)).findBaseCurrency()
        verify(currencyMapper, times(1)).invoke(eq(testBaseCurrencyEntity))
    }

    @Test
    fun whenNoBaseCurrency_returnsNull() {
        whenever(currencyDao.findBaseCurrency()) doReturn null

        val baseCurrency = currencyStorage.loadBaseCurrency()
        assertThat(baseCurrency).isNull()
        verify(currencyMapper, times(0)).invoke(any())
    }

    @Test
    fun whenSaveBaseCurrency_itIsSaved() {
        whenever(currencyEntityMapper.invoke(testCurrency, null)) doReturn testBaseCurrencyEntity

        currencyStorage.saveBaseCurrency(testCurrency)

        verify(currencyEntityMapper, times(1)).invoke(eq(testCurrency), eq(null))
        verify(currencyDao, times(1)).saveBaseCurrency(eq(testBaseCurrencyEntity))
    }
}