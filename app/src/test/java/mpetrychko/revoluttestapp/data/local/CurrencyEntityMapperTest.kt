package mpetrychko.revoluttestapp.data.local

import com.google.common.truth.Truth
import mpetrychko.revoluttestapp.domain.Currency
import org.junit.Test

class CurrencyEntityMapperTest {

    private val currencyEntityMapper = CurrencyEntityMapper()

    @Test
    fun mapCurrencyReturnsCorrectValues() {
        val currency = Currency("a", 2.333, isBaseCurrency = false)
        val parent = "p"
        val currencyEntity = currencyEntityMapper(currency, parent)
        Truth.assertThat(currencyEntity.currencyName).isEqualTo(currency.currencyName)
        Truth.assertThat(currencyEntity.rate).isEqualTo(currency.currencyRate)
        Truth.assertThat(currencyEntity.isBaseCurrency).isEqualTo(currency.isBaseCurrency)
        Truth.assertThat(currencyEntity.parent).isEqualTo(parent)
    }

    @Test
    fun mapCurrencyWithNullParent_returnsEmptyParent() {
        val currency = Currency("a", 2.333, isBaseCurrency = false)
        val currencyEntity = currencyEntityMapper(currency, null)
        Truth.assertThat(currencyEntity.parent).isEmpty()
    }
}