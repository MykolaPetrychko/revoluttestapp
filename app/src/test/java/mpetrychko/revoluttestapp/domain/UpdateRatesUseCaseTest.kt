package mpetrychko.revoluttestapp.domain

import com.google.common.truth.Truth.assertThat
import com.nhaarman.mockito_kotlin.*
import io.reactivex.Observable
import io.reactivex.Single
import mpetrychko.revoluttestapp.domain.UpdateRatesUseCase.*
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import java.util.concurrent.TimeoutException

class UpdateRatesUseCaseTest {

    @Mock
    private lateinit var currencyDataSource: CurrencyDataSource

    @Mock
    private lateinit var currencyStorage: CurrencyStorage

    @Mock
    private lateinit var connectivityProvider: ConnectivityProvider

    private lateinit var updateRatesUseCase: UpdateRatesUseCase

    private lateinit var closeable: AutoCloseable

    private val oldTestBaseCurrency = Currency("c2", 1.0, true)
    private val newTestBaseCurrency = Currency("c5", 1.0, false)
    private val oldTargetTestCurrencies = arrayListOf(
        Currency("c3", 1.0, false),
        newTestBaseCurrency,
        Currency("c4", 1.0, false)
    )
    private val newTargetTestCurrencies = arrayListOf(
        oldTestBaseCurrency.copy(isBaseCurrency = false),
        Currency("c3", 1.0, false),
        Currency("c4", 1.0, false)
    )
    private val oldRelativeCurrencyTest = RelativeCurrency(
        oldTestBaseCurrency,
        oldTargetTestCurrencies
    )
    private val newRelativeCurrencyTest = RelativeCurrency(
        newTestBaseCurrency,
        newTargetTestCurrencies
    )

    @Before
    fun init() {
        closeable = MockitoAnnotations.openMocks(this)

        whenever(currencyDataSource.loadCurrencyRelativeRate(any())) doReturn Single.fromCallable { oldRelativeCurrencyTest }

        updateRatesUseCase =
            spy(UpdateRatesUseCase(currencyDataSource, currencyStorage, connectivityProvider))
    }

    @After
    @Throws(Exception::class)
    fun tearDown() {
        closeable.close()
    }

    @Test
    fun whenRateActionIsChangeBaseCurrency_noPrevState_verifyNewBaseCurrencyIsSet() {
        val expectedState = RatesState(relativeCurrency = oldRelativeCurrencyTest)
        doReturn(Observable.fromCallable { expectedState }).`when`(
            updateRatesUseCase
        ).eachSecondPolling(eq(newTestBaseCurrency.copy(isBaseCurrency = true)))

        val testObserver =
            updateRatesUseCase.baseCurrencySubscriber()(
                ChangeCurrentBaseCurrencyAction(
                    newTestBaseCurrency
                )
            ).test()
        testObserver.assertNoErrors()
        testObserver.assertComplete()
        testObserver.assertValueCount(1)

        val newState = testObserver.values().first()
        assertThat(newState).isEqualTo(expectedState)

        verify(
            currencyStorage,
            times(1)
        ).saveBaseCurrency(eq(newTestBaseCurrency.copy(isBaseCurrency = true)))
        verify(updateRatesUseCase, times(1)).eachSecondPolling(
            eq(
                newTestBaseCurrency.copy(
                    isBaseCurrency = true
                )
            )
        )
    }

    @Test
    fun whenRateActionIsStartTracking_noPrevSavedBaseCurrencyHasPrevState_verifyPrevStateReturnedNetworkRequested() {
        val networkState = RatesState(relativeCurrency = oldRelativeCurrencyTest)
        val defaultBaseCurrency = updateRatesUseCase.defaultBaseCurrency
        doReturn(Observable.fromCallable { networkState }).`when`(
            updateRatesUseCase
        ).eachSecondPolling(eq(defaultBaseCurrency))
        doReturn(newTargetTestCurrencies).`when`(updateRatesUseCase).mergeNewBaseCurrency(
            oldRelativeCurrencyTest.targetCurrencies,
            oldRelativeCurrencyTest.baseCurrency,
            defaultBaseCurrency
        )
        updateRatesUseCase.currentRatesState.set(RatesState(relativeCurrency = oldRelativeCurrencyTest))

        val testObserver =
            updateRatesUseCase.baseCurrencySubscriber()(StartTrackingRateAction).test()
        testObserver.assertNoErrors()
        testObserver.assertComplete()
        testObserver.assertValueCount(2)

        val prevState = testObserver.values().first()
        assertThat(prevState.relativeCurrency?.targetCurrencies).isEqualTo(newTargetTestCurrencies)
        assertThat(prevState.relativeCurrency?.baseCurrency).isEqualTo(defaultBaseCurrency)

        val newNetworkState = testObserver.values()[1]
        assertThat(newNetworkState).isEqualTo(networkState)

        verify(updateRatesUseCase, times(1)).mergeNewBaseCurrency(
            eq(oldRelativeCurrencyTest.targetCurrencies),
            eq(oldRelativeCurrencyTest.baseCurrency),
            eq(defaultBaseCurrency)
        )
        verify(updateRatesUseCase, times(1)).eachSecondPolling(defaultBaseCurrency)
    }

    @Test
    fun whenRateActionIsStartTracking_noPrevSavedBaseCurrencyNoPrevState_verifyDefaultBaseCurrencyIsSet() {
        val expectedState = RatesState(relativeCurrency = oldRelativeCurrencyTest)
        val defaultBaseCurrency = updateRatesUseCase.defaultBaseCurrency
        doReturn(Observable.fromCallable { expectedState }).`when`(
            updateRatesUseCase
        ).eachSecondPolling(eq(defaultBaseCurrency))

        val testObserver =
            updateRatesUseCase.baseCurrencySubscriber()(StartTrackingRateAction).test()

        testObserver.assertNoErrors()
        testObserver.assertComplete()
        testObserver.assertValueCount(1)

        val newState = testObserver.values().first()
        assertThat(newState).isEqualTo(expectedState)

        verify(
            currencyStorage,
            times(1)
        ).saveBaseCurrency(eq(defaultBaseCurrency))
        verify(updateRatesUseCase, times(1)).eachSecondPolling(defaultBaseCurrency)
    }

    @Test
    fun whenLoadRates_newStateHasNoDataCanUsePrevState_usePrevState() {
        doReturn(Observable.fromCallable { RatesState(showNoDataAvailableAtTheMoment = true) }).`when`(
            updateRatesUseCase
        )
            .loadAndCacheRelativeCurrency(oldTestBaseCurrency)
        val newExpectedState = RatesState(relativeCurrency = newRelativeCurrencyTest)
        val toBeProcessedState = RatesState(
            relativeCurrency = oldRelativeCurrencyTest,
            showNoDataAvailableAtTheMoment = true
        )
        doReturn(newExpectedState).`when`(updateRatesUseCase).processRates(
            toBeProcessedState, true
        )
        updateRatesUseCase.currentRatesState.set(RatesState(relativeCurrency = oldRelativeCurrencyTest))

        val testObserver = updateRatesUseCase.loadRates(oldTestBaseCurrency).test()
        testObserver.assertNoErrors()
        testObserver.assertComplete()
        testObserver.assertValueCount(2)

        val loadingState = testObserver.values().first()
        assertThat(loadingState.isLoading).isTrue()

        val newState = testObserver.values()[1]
        assertThat(newState).isEqualTo(newExpectedState)

        verify(updateRatesUseCase, times(1)).processRates(eq(toBeProcessedState), eq(true))
    }

    @Test
    fun whenLoadRates_hasMemory_memoryReturnedAndNetworkRequested() {
        updateRatesUseCase.baseCurrencyCache[newTestBaseCurrency.currencyName.orEmpty()] =
            newRelativeCurrencyTest
        val memoryRate = RatesState(relativeCurrency = newRelativeCurrencyTest)
        doReturn(Observable.fromCallable { memoryRate }).`when`(updateRatesUseCase)
            .loadCachedRates(newRelativeCurrencyTest, newTestBaseCurrency)

        val testObserver = updateRatesUseCase.loadRates(newTestBaseCurrency).test()

        testObserver.assertNoErrors()
        testObserver.assertComplete()
        testObserver.assertValueCount(1)

        val newState = testObserver.values().first()
        assertThat(newState.relativeCurrency).isEqualTo(newRelativeCurrencyTest)

        verify(updateRatesUseCase, times(1)).loadCachedRates(
            newRelativeCurrencyTest,
            newTestBaseCurrency
        )
    }

    @Test
    fun whenLoadRates_hasStorage_storageReturnedAndNetworkRequested() {
        doReturn(oldRelativeCurrencyTest).`when`(currencyStorage)
            .loadRelativeCurrency(newTestBaseCurrency)
        val storageRate = RatesState(relativeCurrency = oldRelativeCurrencyTest)
        doReturn(Observable.fromCallable { storageRate }).`when`(updateRatesUseCase)
            .loadCachedRates(oldRelativeCurrencyTest, newTestBaseCurrency)

        val testObserver = updateRatesUseCase.loadRates(newTestBaseCurrency).test()

        testObserver.assertNoErrors()
        testObserver.assertComplete()
        testObserver.assertValueCount(1)

        val newState = testObserver.values().first()
        assertThat(newState.relativeCurrency).isEqualTo(oldRelativeCurrencyTest)

        verify(updateRatesUseCase, times(1)).loadCachedRates(
            oldRelativeCurrencyTest,
            newTestBaseCurrency
        )
    }

    @Test
    fun whenLoadRates_noCacheNoStorageHasPrevState_newStateIsLoadingAndHasPrevState() {
        val networkState = RatesState()
        whenever(
            updateRatesUseCase.loadAndCacheRelativeCurrency(
                newTestBaseCurrency,
                null
            )
        ) doReturn Observable.fromCallable { networkState }
        doReturn(networkState).`when`(updateRatesUseCase).processRates(networkState, false)
        updateRatesUseCase.currentRatesState.set(RatesState(relativeCurrency = oldRelativeCurrencyTest))

        val testObserver = updateRatesUseCase.loadRates(newTestBaseCurrency).test()

        testObserver.assertNoErrors()
        testObserver.assertComplete()
        testObserver.assertValueCount(2)

        val newState = testObserver.values().first()
        assertThat(newState.isLoading).isTrue()
        assertThat(newState.showNoDataAvailableAtTheMoment).isFalse()
        assertThat(newState.relativeCurrency).isEqualTo(oldRelativeCurrencyTest)

        verify(updateRatesUseCase, times(1)).loadAndCacheRelativeCurrency(
            newTestBaseCurrency,
            null
        )
    }

    @Test
    fun whenLoadRates_noCacheNoStorageNoPrevState_newStateIsLoading() {
        val networkState = RatesState()
        whenever(
            updateRatesUseCase.loadAndCacheRelativeCurrency(
                newTestBaseCurrency,
                null
            )
        ) doReturn Observable.fromCallable { networkState }
        doReturn(networkState).`when`(updateRatesUseCase).processRates(networkState, false)

        val testObserver = updateRatesUseCase.loadRates(newTestBaseCurrency).test()

        testObserver.assertNoErrors()
        testObserver.assertComplete()
        testObserver.assertValueCount(2)

        val newState = testObserver.values().first()
        assertThat(newState.isLoading).isTrue()

        val targetNetworkState = testObserver.values()[1]
        assertThat(targetNetworkState).isEqualTo(networkState)
    }

    @Test
    fun whenLoadCachedRates_cachedRatesAreProcessedAndNewRatesAreRequested() {
        val newState = RatesState(newRelativeCurrencyTest)
        val cachedCurrencies = oldRelativeCurrencyTest
        whenever(
            updateRatesUseCase.loadAndCacheRelativeCurrency(
                newTestBaseCurrency,
                cachedCurrencies.copy(baseCurrency = newTestBaseCurrency)
            )
        ) doReturn Observable.fromCallable { newState }

        val testObserver =
            updateRatesUseCase.loadCachedRates(cachedCurrencies, newTestBaseCurrency).test()
        testObserver.assertNoErrors()
        testObserver.assertComplete()
        testObserver.assertValueCount(2)

        val cachedState = testObserver.values().first()
        assertThat(cachedState.isLoading).isTrue()
        assertThat(cachedState.relativeCurrency).isNotNull()
        assertThat(cachedState.relativeCurrency?.baseCurrency).isEqualTo(newTestBaseCurrency)
        assertThat(cachedState.relativeCurrency?.targetCurrencies).isEqualTo(cachedCurrencies.targetCurrencies)

        val networkState = testObserver.values()[1]
        assertThat(networkState).isEqualTo(newState)
    }

    @Test
    fun whenLoadAndCacheRelativeCurrency_noInternetConnectionHasCachedValue_returnsCachedValue() {
        whenever(connectivityProvider.isConnectedToTheInternet()) doReturn false

        val testObserver =
            updateRatesUseCase.loadAndCacheRelativeCurrency(
                oldTestBaseCurrency,
                newRelativeCurrencyTest
            ).test()
        testObserver.assertValueCount(1)
        testObserver.assertComplete()
        testObserver.assertNoErrors()

        val state = testObserver.values().first()
        assertThat(state.relativeCurrency).isNotNull()
        assertThat(state.relativeCurrency).isEqualTo(newRelativeCurrencyTest)
        assertThat(state.showNetworkError).isTrue()
        assertThat(state.showNoDataAvailableAtTheMoment).isFalse()
    }

    @Test
    fun whenLoadAndCacheRelativeCurrency_hasInternetGotExceptionNoCachedValue_sendNoDataAvailable() {
        whenever(connectivityProvider.isConnectedToTheInternet()) doReturn true
        whenever(currencyDataSource.loadCurrencyRelativeRate(any())) doReturn Single.error(Exception())

        val testObserver =
            updateRatesUseCase.loadAndCacheRelativeCurrency(oldTestBaseCurrency).test()
        testObserver.assertValueCount(1)
        testObserver.assertComplete()
        testObserver.assertNoErrors()

        val state = testObserver.values().first()
        assertThat(state.showNoDataAvailableAtTheMoment).isTrue()
    }

    @Test
    fun whenLoadAndCacheRelativeCurrency_hasInternetGotNetworkExceptionNoCachedValue_returnsNetworkError() {
        whenever(connectivityProvider.isConnectedToTheInternet()) doReturn true
        whenever(currencyDataSource.loadCurrencyRelativeRate(any())) doReturn Single.error(
            TimeoutException()
        )
        val testObserver =
            updateRatesUseCase.loadAndCacheRelativeCurrency(oldTestBaseCurrency).test()
        testObserver.assertValueCount(1)
        testObserver.assertComplete()
        testObserver.assertNoErrors()

        val state = testObserver.values().first()
        assertThat(state.relativeCurrency).isNull()
        assertThat(state.showNetworkError).isTrue()
        assertThat(state.showUnknownError).isFalse()
        assertThat(state.showNoDataAvailableAtTheMoment).isTrue()
    }

    @Test
    fun whenLoadAndCacheRelativeCurrency_hasInternetGotExceptionNoCachedValue_returnsError() {
        whenever(connectivityProvider.isConnectedToTheInternet()) doReturn true
        whenever(currencyDataSource.loadCurrencyRelativeRate(any())) doReturn Single.error(Exception())
        val testObserver =
            updateRatesUseCase.loadAndCacheRelativeCurrency(oldTestBaseCurrency).test()
        testObserver.assertValueCount(1)
        testObserver.assertComplete()
        testObserver.assertNoErrors()

        val state = testObserver.values().first()
        assertThat(state.relativeCurrency).isNull()
        assertThat(state.showNetworkError).isFalse()
        assertThat(state.showUnknownError).isTrue()
        assertThat(state.showNoDataAvailableAtTheMoment).isTrue()
    }

    @Test
    fun whenLoadAndCacheRelativeCurrency_hasInternet_savesResultToCache() {
        whenever(connectivityProvider.isConnectedToTheInternet()) doReturn true
        val testObserver =
            updateRatesUseCase.loadAndCacheRelativeCurrency(oldTestBaseCurrency).test()
        testObserver.assertValueCount(1)
        testObserver.assertComplete()
        testObserver.assertNoErrors()

        verify(currencyStorage, times(1)).saveRelativeCurrency(eq(oldRelativeCurrencyTest))
        assertThat(updateRatesUseCase.baseCurrencyCache.containsKey(oldRelativeCurrencyTest.baseCurrency.currencyName.orEmpty())).isTrue()
        assertThat(updateRatesUseCase.baseCurrencyCache[oldRelativeCurrencyTest.baseCurrency.currencyName.orEmpty()]).isEqualTo(
            oldRelativeCurrencyTest
        )
    }

    @Test
    fun whenLoadAndCacheRelativeCurrency_noInternetConnectionNoCachedValue_returnsNetworkError() {
        whenever(connectivityProvider.isConnectedToTheInternet()) doReturn false

        val testObserver =
            updateRatesUseCase.loadAndCacheRelativeCurrency(oldTestBaseCurrency).test()
        testObserver.assertValueCount(1)
        testObserver.assertComplete()
        testObserver.assertNoErrors()

        val state = testObserver.values().first()
        assertThat(state.relativeCurrency).isNull()
        assertThat(state.showNetworkError).isTrue()
        assertThat(state.showNoDataAvailableAtTheMoment).isTrue()
    }

    @Test
    fun whenProcessRates_hasInternet_returnsCorrectState() {
        whenever(connectivityProvider.isConnectedToTheInternet()) doReturn true
        val targetState = RatesState(showIsWorkingInOfflineMode = true)
        val newState = updateRatesUseCase.processRates(targetState, false)
        assertThat(newState.showIsWorkingInOfflineMode).isFalse()
    }

    @Test
    fun whenProcessRates_noInternet_returnsCorrectState() {
        whenever(connectivityProvider.isConnectedToTheInternet()) doReturn false
        val targetState = RatesState(showIsWorkingInOfflineMode = false)
        val newState = updateRatesUseCase.processRates(targetState, false)
        assertThat(newState.showIsWorkingInOfflineMode).isTrue()
    }

    @Test
    fun whenProcessRates_prevStateIsNull_returnNewState() {
        val targetState = RatesState(
            relativeCurrency = newRelativeCurrencyTest,
            showNoDataAvailableAtTheMoment = false,
            showUnknownError = false,
            showNetworkError = true,
            isLoading = true,
            showIsWorkingInOfflineMode = true
        )
        val newState = updateRatesUseCase.processRates(targetState, false)
        assertThat(newState.showNoDataAvailableAtTheMoment).isEqualTo(targetState.showNoDataAvailableAtTheMoment)
        assertThat(newState.isLoading).isEqualTo(targetState.isLoading)
        assertThat(newState.showNetworkError).isEqualTo(targetState.showNetworkError)
        assertThat(newState.showUnknownError).isEqualTo(targetState.showUnknownError)
    }

    @Test
    fun whenProcessRates_hasPrevState_mergeRatesIsCalled() {
        val targetState = RatesState()
        val prevState = RatesState()
        updateRatesUseCase.currentRatesState.set(prevState)
        updateRatesUseCase.processRates(targetState, true)
        verify(updateRatesUseCase, times(1)).mergeStates(prevState, targetState, true)
    }

    @Test
    fun whenProcessRates_forceStaticRates_mergeRatesIsCalled() {
        val targetState = RatesState()
        updateRatesUseCase.processRates(targetState, true)
        verify(updateRatesUseCase, times(1)).mergeStates(null, targetState, true)
    }

    @Test
    fun whenProcessRates_doNotForceStaticRates_mergeRatesIsCalled() {
        val targetState = RatesState()
        updateRatesUseCase.processRates(targetState, false)
        verify(updateRatesUseCase, times(1)).mergeStates(null, targetState, false)
    }

    @Test
    fun whenMergeStates_forceStaticRatesTheSameCurrencies_oldRatesReturned() {
        val newState = updateRatesUseCase.mergeStates(
            RatesState(relativeCurrency = oldRelativeCurrencyTest),
            RatesState(
                relativeCurrency = RelativeCurrency(
                    oldTestBaseCurrency,
                    newTargetTestCurrencies
                )
            ),
            forceStaticRates = true
        )

        verify(updateRatesUseCase, times(0)).updateCurrencyRate(
            any(),
            any(),
            any()
        )
        verify(updateRatesUseCase, times(0)).mergeNewBaseCurrency(
            any(),
            any(),
            any()
        )

        assertThat(newState?.relativeCurrency?.targetCurrencies).isEqualTo(oldRelativeCurrencyTest.targetCurrencies)
    }

    @Test
    fun whenMergeStates_forceStaticRates_oldRatesReturned() {
        whenever(
            updateRatesUseCase.mergeNewBaseCurrency(
                newTargetTestCurrencies,
                oldRelativeCurrencyTest.baseCurrency,
                newRelativeCurrencyTest.baseCurrency
            )
        ) doReturn newTargetTestCurrencies

        val newState = updateRatesUseCase.mergeStates(
            RatesState(relativeCurrency = oldRelativeCurrencyTest),
            RatesState(relativeCurrency = newRelativeCurrencyTest),
            forceStaticRates = true
        )

        verify(updateRatesUseCase, times(0)).updateCurrencyRate(
            any(),
            any(),
            any()
        )

        assertThat(newState?.relativeCurrency?.targetCurrencies).isEqualTo(newTargetTestCurrencies)
    }

    @Test
    fun whenMergeStates_currenciesAreDifferent_newStateHasCorrectValues() {
        val targetNewState = RatesState(
            relativeCurrency = newRelativeCurrencyTest,
            showNoDataAvailableAtTheMoment = false,
            showUnknownError = false,
            showNetworkError = true,
            showIsWorkingInOfflineMode = true
        )
        val newState = updateRatesUseCase.mergeStates(
            RatesState(relativeCurrency = oldRelativeCurrencyTest),
            targetNewState
        )

        assertThat(newState).isNotNull()
        assertThat(newState?.showNoDataAvailableAtTheMoment).isEqualTo(targetNewState.showNoDataAvailableAtTheMoment)
        assertThat(newState?.isLoading).isEqualTo(targetNewState.isLoading)
        assertThat(newState?.showIsWorkingInOfflineMode).isEqualTo(targetNewState.showIsWorkingInOfflineMode)
        assertThat(newState?.showNetworkError).isEqualTo(targetNewState.showNetworkError)
        assertThat(newState?.showUnknownError).isEqualTo(targetNewState.showUnknownError)
        assertThat(newState?.relativeCurrency).isNotNull()
        assertThat(newState?.relativeCurrency?.baseCurrency).isEqualTo(newRelativeCurrencyTest.baseCurrency)
        assertThat(newState?.relativeCurrency?.targetCurrencies?.find { it.currencyName == oldRelativeCurrencyTest.baseCurrency.currencyName }).isNotNull()
        assertThat(newState?.relativeCurrency?.targetCurrencies).isNotEqualTo(
            oldRelativeCurrencyTest.targetCurrencies
        )
    }

    @Test
    fun whenMergeStates_currenciesAreDifferent_mergeUpdateCurrenciesAreCalled() {
        whenever(
            updateRatesUseCase.mergeNewBaseCurrency(
                newTargetTestCurrencies,
                oldRelativeCurrencyTest.baseCurrency,
                newRelativeCurrencyTest.baseCurrency
            )
        ) doReturn newTargetTestCurrencies

        updateRatesUseCase.mergeStates(
            RatesState(relativeCurrency = oldRelativeCurrencyTest),
            RatesState(relativeCurrency = newRelativeCurrencyTest)
        )

        verify(
            updateRatesUseCase,
            times(1)
        ).mergeNewBaseCurrency(
            eq(oldRelativeCurrencyTest.targetCurrencies),
            eq(oldRelativeCurrencyTest.baseCurrency),
            eq(newRelativeCurrencyTest.baseCurrency)
        )
        verify(updateRatesUseCase, times(1)).updateCurrencyRate(
            eq(newTargetTestCurrencies),
            eq(newRelativeCurrencyTest.targetCurrencies),
            eq(newRelativeCurrencyTest.baseCurrency)
        )
    }

    @Test
    fun whenMergeStates_newStateDataNotAvailable_returnOldState() {
        val oldState = RatesState(relativeCurrency = oldRelativeCurrencyTest)
        val newState = updateRatesUseCase.mergeStates(
            oldState, RatesState(showNoDataAvailableAtTheMoment = true)
        )
        assertThat(newState?.showNoDataAvailableAtTheMoment).isTrue()
        assertThat(newState?.isLoading).isEqualTo(oldState.isLoading)
        assertThat(newState?.relativeCurrency).isEqualTo(oldState.relativeCurrency)
        assertThat(newState?.showIsWorkingInOfflineMode).isEqualTo(oldState.showIsWorkingInOfflineMode)
        assertThat(newState?.showNetworkError).isEqualTo(oldState.showNetworkError)
        assertThat(newState?.showUnknownError).isEqualTo(oldState.showUnknownError)
    }

    @Test
    fun whenMergeStates_newCurrencyIsNull_returnNull() {
        val newState = updateRatesUseCase.mergeStates(
            RatesState(relativeCurrency = oldRelativeCurrencyTest),
            RatesState()
        )
        assertThat(newState).isNull()
    }

    @Test
    fun whenMergeStates_oldCurrencyIsNull_returnNull() {
        val newState = updateRatesUseCase.mergeStates(
            RatesState(),
            RatesState(relativeCurrency = oldRelativeCurrencyTest)
        )
        assertThat(newState).isNull()
    }

    @Test
    fun whenMergeStates_relativeCurrencyIsNull_returnNull() {
        val newState = updateRatesUseCase.mergeStates(RatesState(), RatesState())
        assertThat(newState).isNull()
    }

    @Test
    fun whenMergeStates_oldStateIsNull_returnNull() {
        val newState = updateRatesUseCase.mergeStates(null, RatesState())
        assertThat(newState).isNull()
    }

    @Test
    fun whenMergeStates_newStateIsNull_returnNull() {
        val newState = updateRatesUseCase.mergeStates(RatesState(), null)
        assertThat(newState).isNull()
    }

    @Test
    fun whenMergeStates_statesAreNull_returnNull() {
        val newState = updateRatesUseCase.mergeStates(null, null)
        assertThat(newState).isNull()
    }

    @Test
    fun whenUpdateCurrencyRate_verifyItIsUpdatedCorrectly() {
        val oldCurrencies = arrayListOf(Currency("c1", 2.0), Currency("c2", 3.5))
        val newCurrenciesRates = arrayListOf(Currency("c1", 2.0), Currency("c2", 2.0))
        val targetFactorCurrency = Currency("c3", 5.0)
        val expectedCurrenciesRatesMap = mapOf("c1" to 10.0, "c2" to 10.0)
        val updated = updateRatesUseCase.updateCurrencyRate(
            oldCurrencies,
            newCurrenciesRates,
            targetFactorCurrency
        )
        assertThat(updated).isNotEmpty()
        assertThat(updated.size).isEqualTo(oldCurrencies.size)
        val updatedMap = updated.map { it.currencyName to it.currencyRate }.toMap()
        oldCurrencies.forEach {
            assertThat(updatedMap[it.currencyName.orEmpty()]).isNotNull()
            assertThat(updatedMap[it.currencyName.orEmpty()]).isEqualTo(
                expectedCurrenciesRatesMap[it.currencyName.orEmpty()]
            )
        }
    }

    @Test
    fun whenMergeEmptyCurrencies_verifyNoMergeIsDone() {
        val mergedCurrencies = updateRatesUseCase.mergeNewBaseCurrency(
            arrayListOf(),
            oldTestBaseCurrency,
            newTestBaseCurrency
        )
        assertThat(mergedCurrencies).isEmpty()
    }

    @Test
    fun whenMergeCurrencies_CannotFindNewCurrency_verifyNoMergeIsDone() {
        val toBeMerged = arrayListOf(oldTestBaseCurrency)
        val mergedCurrencies = updateRatesUseCase.mergeNewBaseCurrency(
            toBeMerged,
            oldTestBaseCurrency,
            newTestBaseCurrency
        )
        assertThat(mergedCurrencies).isEqualTo(toBeMerged)
    }

    @Test
    fun whenMergeCurrencies_verifyItIsMerged() {
        val mergedCurrencies = updateRatesUseCase.mergeNewBaseCurrency(
            oldTargetTestCurrencies,
            oldTestBaseCurrency,
            newTestBaseCurrency
        )
        assertThat(mergedCurrencies).isNotEmpty()
        assertThat(mergedCurrencies.size).isEqualTo(oldTargetTestCurrencies.size)
        assertThat(mergedCurrencies.first()).isEqualTo(oldTestBaseCurrency.copy(isBaseCurrency = false))
        assertThat(mergedCurrencies.find { it == newTestBaseCurrency }).isNull()
    }
}