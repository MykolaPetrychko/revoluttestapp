package mpetrychko.revoluttestapp.ext

import com.google.common.truth.Truth.assertThat
import org.junit.Before
import org.junit.Test
import java.text.NumberFormat

class NumberExtKtTest {

    private lateinit var numberFormat: NumberFormat

    @Before
    fun init() {
        numberFormat = NumberFormat.getNumberInstance().apply {
            maximumFractionDigits = 2
            isGroupingUsed = false
        }
    }

    @Test
    fun whenFormatNumber_verifyItIsFormatted() {
        val formatted = numberFormat.formatNullable(1.2)
        assertThat(formatted.isNullOrBlank()).isFalse()
    }

    @Test
    fun whenFormatNull_verifyNullIsReturned() {
        val formatted = numberFormat.formatNullable(null)
        assertThat(formatted).isNull()
    }

    @Test
    fun whenParseANumber_verifyItIsParsed() {
        val parsed = numberFormat.parseNullable("123")
        assertThat(parsed).isNotNull()
    }

    @Test
    fun whenParseNullable_verifyNullIsReturned() {
        val parsed = numberFormat.parseNullable(null)
        assertThat(parsed).isNull()
    }

    @Test
    fun whenParseADot_verifyNullIsReturned() {
        val parsed = numberFormat.parseNullable(".")
        assertThat(parsed).isNull()
    }

    @Test
    fun whenParseNotANumber_verifyNullIsReturned() {
        val parsed = numberFormat.parseNullable("one")
        assertThat(parsed).isNull()
    }
}