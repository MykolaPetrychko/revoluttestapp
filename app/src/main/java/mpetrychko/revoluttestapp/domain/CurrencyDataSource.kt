package mpetrychko.revoluttestapp.domain

import io.reactivex.Single

interface CurrencyDataSource {
    fun loadCurrencyRelativeRate(baseCurrency: Currency): Single<RelativeCurrency>
}