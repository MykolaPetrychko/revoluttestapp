package mpetrychko.revoluttestapp.domain

interface ConnectivityProvider {
    fun isConnectedToTheInternet(): Boolean
}