package mpetrychko.revoluttestapp.domain

data class Currency(
    val currencyName: String?,
    val currencyRate: Double?,
    val isBaseCurrency: Boolean = false
)

data class RelativeCurrency(val baseCurrency: Currency, val targetCurrencies: List<Currency>)