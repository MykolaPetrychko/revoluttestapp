package mpetrychko.revoluttestapp.domain

import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import java.net.UnknownHostException
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeoutException
import java.util.concurrent.atomic.AtomicReference

class UpdateRatesUseCase(
    private val currencyDataSource: CurrencyDataSource,
    private val currencyStorage: CurrencyStorage,
    private val connectivityProvider: ConnectivityProvider
) {

    internal val defaultBaseCurrency = Currency(
        "EUR",
        100.0,
        isBaseCurrency = true
    )

    internal val currentRatesState = AtomicReference<RatesState>()

    internal val currentBaseCurrency = AtomicReference<Currency>()

    internal val baseCurrencyCache = ConcurrentHashMap<String, RelativeCurrency>()

    fun updateRates(
        baseCurrencyObservable: Observable<RatesAction>
    ): Observable<RatesState> {
        return baseCurrencyObservable
            .debounce(200, TimeUnit.MILLISECONDS)
            .observeOn(Schedulers.io())
            .switchMap(baseCurrencySubscriber())
            .doOnNext { currentRatesState.set(it) }
    }

    internal fun baseCurrencySubscriber(): (RatesAction) -> Observable<RatesState> =
        f@{ currencyAction ->
            val baseCurrency = when (currencyAction) {
                is StartTrackingRateAction -> currentBaseCurrency.get()
                    ?: currencyStorage.loadBaseCurrency()
                    ?: defaultBaseCurrency
                is StopTrackingRateAction -> return@f Observable.empty<RatesState>()
                is ChangeCurrentBaseCurrencyAction -> currencyAction.newBaseCurrency.copy(
                    isBaseCurrency = true
                )
            }

            currentBaseCurrency.set(baseCurrency)
            currencyStorage.saveBaseCurrency(baseCurrency)

            val prevState = currentRatesState.get()
            if (prevState?.relativeCurrency != null) {
                val newState =
                    prevState.copy(
                        relativeCurrency = RelativeCurrency(
                            baseCurrency = baseCurrency,
                            targetCurrencies = mergeNewBaseCurrency(
                                prevState.relativeCurrency.targetCurrencies,
                                prevState.relativeCurrency.baseCurrency,
                                baseCurrency
                            )
                        ),
                        showNoDataAvailableAtTheMoment = false
                    )
                return@f eachSecondPolling(baseCurrency)
                    .subscribeOn(Schedulers.io())
                    .startWith(newState)
                    .distinctUntilChanged()
            }

            eachSecondPolling(baseCurrency)
                .subscribeOn(Schedulers.io())
                .startWith(RatesState(isLoading = true))
                .distinctUntilChanged()
        }

    internal fun eachSecondPolling(baseCurrency: Currency): Observable<RatesState> =
        Observable.concat(
            loadRates(baseCurrency, loadRatesFromCache(baseCurrency)),
            Flowable.interval(1, 1, TimeUnit.SECONDS)
                .onBackpressureLatest()
                .observeOn(Schedulers.io(), false, 1)
                .filter { baseCurrency.currencyRate != null }
                .filter {
                    val connected = connectivityProvider.isConnectedToTheInternet()
                    connected || (!connected && currentRatesState.get()?.showIsWorkingInOfflineMode == false)
                }
                .flatMap {
                    loadRates(
                        baseCurrency,
                        loadRatesFromNetworkOnly(baseCurrency)
                    ).toFlowable(BackpressureStrategy.LATEST)
                }
                .toObservable()
        )

    internal fun loadRates(
        baseCurrency: Currency,
        sourceObservable: Observable<RatesState> = loadRatesFromCache(
            baseCurrency
        )
    ) =
        sourceObservable.map { newState ->
            val currentState = currentRatesState.get()
            if (newState.showNoDataAvailableAtTheMoment && currentState != null) {
                return@map processRates(
                    newState.copy(relativeCurrency = currentState.relativeCurrency),
                    forceStaticRates = true
                )
            }
            processRates(newState)
        }

    internal fun loadRatesFromCache(baseCurrency: Currency) = Observable.defer {
        val cachedRelativeCurrency = baseCurrencyCache[baseCurrency.currencyName]
        if (cachedRelativeCurrency != null) {
            return@defer loadCachedRates(cachedRelativeCurrency, baseCurrency)
        }
        val storedRelativeCurrency = currencyStorage.loadRelativeCurrency(baseCurrency)
        if (storedRelativeCurrency != null && storedRelativeCurrency.targetCurrencies.isNotEmpty()) {
            return@defer loadCachedRates(storedRelativeCurrency, baseCurrency)
        }
        loadAndCacheRelativeCurrency(baseCurrency)
            .startWith(
                currentRatesState.get()?.copy(
                    isLoading = true,
                    showNoDataAvailableAtTheMoment = false
                ) ?: RatesState(isLoading = true)
            )
    }

    internal fun loadRatesFromNetworkOnly(baseCurrency: Currency) = Observable.defer {
        val cachedRelativeCurrency =
            baseCurrencyCache[baseCurrency.currencyName] ?: currencyStorage.loadRelativeCurrency(
                baseCurrency
            )

        loadAndCacheRelativeCurrency(
            baseCurrency,
            if (cachedRelativeCurrency?.targetCurrencies?.isEmpty() == true) null else cachedRelativeCurrency
        )
    }

    internal fun loadCachedRates(
        cachedRelativeCurrency: RelativeCurrency,
        baseCurrency: Currency
    ): Observable<RatesState> {
        val newCurrency = cachedRelativeCurrency.copy(baseCurrency = baseCurrency)
        return loadAndCacheRelativeCurrency(baseCurrency, newCurrency)
            .delay(1000, TimeUnit.MILLISECONDS)
            .startWith(
                RatesState(
                    relativeCurrency = newCurrency,
                    isLoading = true
                )
            )
    }

    internal fun loadAndCacheRelativeCurrency(
        baseCurrency: Currency,
        cachedRelativeCurrency: RelativeCurrency? = null
    ) =
        Single.defer {
            if (connectivityProvider.isConnectedToTheInternet()) {
                return@defer currencyDataSource
                    .loadCurrencyRelativeRate(baseCurrency)
                    .doOnSuccess {
                        baseCurrencyCache[baseCurrency.currencyName.orEmpty()] = it
                        currencyStorage.saveRelativeCurrency(it)
                    }.map { RatesState(relativeCurrency = it) }
                    .onErrorResumeNext { e ->
                        Timber.e(e)
                        val showNetworkError = e is TimeoutException || e is UnknownHostException
                        Single.just(
                            RatesState(
                                relativeCurrency = cachedRelativeCurrency,
                                showNetworkError = showNetworkError,
                                showUnknownError = !showNetworkError,
                                showNoDataAvailableAtTheMoment = cachedRelativeCurrency == null
                            )
                        )
                    }
            }
            Single.just(
                RatesState(
                    relativeCurrency = cachedRelativeCurrency,
                    showNetworkError = true,
                    showNoDataAvailableAtTheMoment = cachedRelativeCurrency == null
                )
            )
        }.toObservable()

    internal fun processRates(
        ratesState: RatesState,
        forceStaticRates: Boolean = false
    ): RatesState {
        val newRates = mergeStates(
            currentRatesState.get(),
            ratesState,
            forceStaticRates
        ) ?: ratesState
        return newRates.copy(showIsWorkingInOfflineMode = !connectivityProvider.isConnectedToTheInternet())
    }

    internal fun mergeStates(
        oldState: RatesState?,
        newState: RatesState?,
        forceStaticRates: Boolean = false
    ): RatesState? {
        if (newState?.showNoDataAvailableAtTheMoment == true && oldState?.relativeCurrency != null) {
            return oldState.copy(
                showNoDataAvailableAtTheMoment = true,
                isLoading = newState.isLoading
            )
        }
        val oldRelativeCurrency =
            oldState?.relativeCurrency ?: newState?.relativeCurrency ?: return null
        return newState?.relativeCurrency?.let { newRelativeCurrency ->
            val newTargetCurrencies =
                if (oldRelativeCurrency.baseCurrency.currencyName != newRelativeCurrency.baseCurrency.currencyName) {
                    mergeNewBaseCurrency(
                        oldRelativeCurrency.targetCurrencies,
                        oldRelativeCurrency.baseCurrency,
                        newRelativeCurrency.baseCurrency
                    )
                } else oldRelativeCurrency.targetCurrencies

            val processedRates = if (forceStaticRates) {
                newTargetCurrencies
            } else {
                updateCurrencyRate(
                    newTargetCurrencies,
                    newRelativeCurrency.targetCurrencies,
                    newRelativeCurrency.baseCurrency
                )
            }
            newState.copy(
                relativeCurrency = RelativeCurrency(
                    targetCurrencies = processedRates,
                    baseCurrency = newRelativeCurrency.baseCurrency
                )
            )
        }
    }

    internal fun updateCurrencyRate(
        oldCurrencies: List<Currency>,
        newCurrencies: List<Currency>,
        targetCurrencyFactor: Currency
    ): List<Currency> {
        val rates =
            newCurrencies.map { it.currencyName to it.currencyRate }
                .toMap()
        val rateFactor = targetCurrencyFactor.currencyRate
        return oldCurrencies.map { c ->
            c.copy(
                currencyRate = rateFactor?.times(
                    rates[c.currencyName] ?: 0.0
                )
            )
        }
    }

    internal fun mergeNewBaseCurrency(
        targetCurrencies: List<Currency>,
        oldBaseCurrency: Currency,
        newBaseCurrency: Currency
    ): List<Currency> {
        val idx =
            targetCurrencies.indexOfFirst { c -> c.currencyName == newBaseCurrency.currencyName }
        return if (idx != -1) {
            val newTarget = targetCurrencies.toMutableList()
            newTarget.removeAt(idx)
            newTarget.add(0, oldBaseCurrency.copy(isBaseCurrency = false))
            newTarget
        } else { // happens only when the base currency wasn't changed
            Timber.i(
                "Couldn't find new base currency %s. Old base currency is %s",
                newBaseCurrency.currencyName,
                oldBaseCurrency.currencyName
            )
            targetCurrencies
        }
    }

    data class RatesState(
        val relativeCurrency: RelativeCurrency? = null,
        val showNetworkError: Boolean = false,
        val showUnknownError: Boolean = false,
        val showIsWorkingInOfflineMode: Boolean = false,
        val isLoading: Boolean = false,
        val showNoDataAvailableAtTheMoment: Boolean = false
    )

    sealed class RatesAction

    object StartTrackingRateAction : RatesAction()

    object StopTrackingRateAction : RatesAction()

    class ChangeCurrentBaseCurrencyAction(val newBaseCurrency: Currency) : RatesAction()
}