package mpetrychko.revoluttestapp.domain

interface CurrencyStorage {
    fun loadRelativeCurrency(baseCurrency: Currency): RelativeCurrency?

    fun saveRelativeCurrency(relativeCurrency: RelativeCurrency)

    fun loadBaseCurrency(): Currency?

    fun saveBaseCurrency(currency: Currency)
}