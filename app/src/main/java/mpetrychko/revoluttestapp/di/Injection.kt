package mpetrychko.revoluttestapp.di

import android.content.Context
import androidx.annotation.MainThread
import androidx.room.Room
import mpetrychko.revoluttestapp.BuildConfig
import mpetrychko.revoluttestapp.ConnectivityManagerProvider
import mpetrychko.revoluttestapp.data.local.CurrencyDatabase
import mpetrychko.revoluttestapp.data.local.CurrencyEntityMapper
import mpetrychko.revoluttestapp.data.local.CurrencyMapper
import mpetrychko.revoluttestapp.data.local.DatabaseCurrencyStorage
import mpetrychko.revoluttestapp.data.remote.CurrencyNetworkDataSource
import mpetrychko.revoluttestapp.data.remote.RatesApi
import mpetrychko.revoluttestapp.domain.CurrencyDataSource
import mpetrychko.revoluttestapp.domain.CurrencyStorage
import mpetrychko.revoluttestapp.domain.UpdateRatesUseCase
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

object Injection {

    private val ratesApi: RatesApi by lazy {
        val client =
            if (BuildConfig.DEBUG) {
                val interceptor = HttpLoggingInterceptor()
                interceptor.level = HttpLoggingInterceptor.Level.BODY
                OkHttpClient.Builder().addInterceptor(interceptor)
                    .build()
            } else OkHttpClient()
        Retrofit.Builder()
            .baseUrl("https://hiring.revolut.codes")
            .client(client)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(RatesApi::class.java)
    }

    private var currencyDb: CurrencyDatabase? = null

    private fun provideRatesApi() = ratesApi

    private fun provideCurrencyDataSource(): CurrencyDataSource =
        CurrencyNetworkDataSource(provideRatesApi())

    @MainThread
    fun provideLocalStorage(context: Context): CurrencyStorage {
        if (currencyDb == null) {
            currencyDb =
                Room.databaseBuilder(context, CurrencyDatabase::class.java, "currency").build()
        }
        return DatabaseCurrencyStorage(
            currencyDb!!.currencyDao(),
            CurrencyEntityMapper(),
            CurrencyMapper()
        )
    }

    @MainThread
    fun provideCurrencyRateUseCase(context: Context) =
        UpdateRatesUseCase(
            provideCurrencyDataSource(),
            provideLocalStorage(context),
            ConnectivityManagerProvider(context)
        )

}