package mpetrychko.revoluttestapp

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import mpetrychko.revoluttestapp.domain.ConnectivityProvider
import timber.log.Timber


class ConnectivityManagerProvider(private val context: Context) : ConnectivityProvider {

    private val connectivityManager by lazy {
        context.getSystemService(
            Context.CONNECTIVITY_SERVICE
        ) as ConnectivityManager
    }

    override fun isConnectedToTheInternet(): Boolean {
        val isConnected = checkConnection()
        Timber.i("isConnected status: %s", isConnected)
        return isConnected
    }

    private fun checkConnection(): Boolean {
        val networkCapabilities =
            connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
        return (networkCapabilities != null && networkCapabilities.hasCapability(NetworkCapabilities.NET_CAPABILITY_VALIDATED)
                && networkCapabilities.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET))
    }
}