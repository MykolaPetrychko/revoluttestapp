package mpetrychko.revoluttestapp.data.remote

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface RatesApi {
    @GET("api/android/latest")
    fun loadRates(@Query("base") baseCurrency: String): Single<RatesResponse>
}