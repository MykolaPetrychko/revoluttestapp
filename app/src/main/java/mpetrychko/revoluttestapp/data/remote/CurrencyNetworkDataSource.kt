package mpetrychko.revoluttestapp.data.remote

import io.reactivex.Single
import mpetrychko.revoluttestapp.domain.Currency
import mpetrychko.revoluttestapp.domain.CurrencyDataSource
import mpetrychko.revoluttestapp.domain.RelativeCurrency

class CurrencyNetworkDataSource(private val ratesApi: RatesApi) : CurrencyDataSource {
    override fun loadCurrencyRelativeRate(baseCurrency: Currency): Single<RelativeCurrency> {
        return ratesApi.loadRates(baseCurrency.currencyName.orEmpty())
            .map { ratesResponse ->
                RelativeCurrency(
                    baseCurrency,
                    ratesResponse.rates.map { Currency(it.key, it.value) })
            }
    }
}