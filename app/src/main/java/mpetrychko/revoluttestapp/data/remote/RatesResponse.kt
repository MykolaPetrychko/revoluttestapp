package mpetrychko.revoluttestapp.data.remote

import com.google.gson.annotations.SerializedName

class RatesResponse(
    @SerializedName("baseCurrency") val baseCurrency: String,
    @SerializedName("rates") val rates: Map<String, Double>
)