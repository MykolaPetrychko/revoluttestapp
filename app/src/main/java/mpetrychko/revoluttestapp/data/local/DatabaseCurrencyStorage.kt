package mpetrychko.revoluttestapp.data.local

import mpetrychko.revoluttestapp.domain.Currency
import mpetrychko.revoluttestapp.domain.CurrencyStorage
import mpetrychko.revoluttestapp.domain.RelativeCurrency

class DatabaseCurrencyStorage(
    private val currencyDao: CurrencyDao,
    private val currencyEntityMapper: (Currency, String?) -> CurrencyEntity,
    private val currencyMapper: (CurrencyEntity) -> Currency
) :
    CurrencyStorage {

    override fun loadRelativeCurrency(baseCurrency: Currency): RelativeCurrency? {
        val targetCurrencies = currencyDao.loadRelativeCurrency(baseCurrency.currencyName.orEmpty())
        return targetCurrencies?.let {
            RelativeCurrency(
                baseCurrency = baseCurrency,
                targetCurrencies = targetCurrencies.map { currencyMapper(it) })
        }
    }

    override fun saveRelativeCurrency(relativeCurrency: RelativeCurrency) {
        currencyDao.insertAll(
            arrayListOf(
                currencyEntityMapper(
                    relativeCurrency.baseCurrency,
                    null
                )
            ) + relativeCurrency.targetCurrencies.map {
                currencyEntityMapper(
                    it,
                    relativeCurrency.baseCurrency.currencyName
                )
            })
    }

    override fun loadBaseCurrency(): Currency? {
        return currencyDao.findBaseCurrency()?.let { currencyMapper(it) }
    }

    override fun saveBaseCurrency(currency: Currency) {
        currencyDao.saveBaseCurrency(currencyEntityMapper(currency, null))
    }
}