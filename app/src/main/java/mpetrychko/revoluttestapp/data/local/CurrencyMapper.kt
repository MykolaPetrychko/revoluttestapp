package mpetrychko.revoluttestapp.data.local

import mpetrychko.revoluttestapp.domain.Currency

class CurrencyMapper : (CurrencyEntity) -> Currency {
    override fun invoke(currencyEntity: CurrencyEntity): Currency {
        return Currency(
            currencyName = currencyEntity.currencyName,
            currencyRate = currencyEntity.rate,
            isBaseCurrency = currencyEntity.isBaseCurrency
        )
    }
}

class CurrencyEntityMapper : (Currency, String?) -> CurrencyEntity {
    override fun invoke(currency: Currency, parent: String?): CurrencyEntity {
        return CurrencyEntity(
            currency.currencyName.orEmpty(),
            parent.orEmpty(),
            currency.currencyRate,
            currency.isBaseCurrency
        )
    }
}