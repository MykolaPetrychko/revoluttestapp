package mpetrychko.revoluttestapp.data.local

import androidx.room.ColumnInfo
import androidx.room.Entity

@Entity(tableName = "currency", primaryKeys = ["currency_name", "parent"])
data class CurrencyEntity(
    @ColumnInfo(name = "currency_name") val currencyName: String,
    val parent: String,
    val rate: Double?,
    @ColumnInfo(name = "is_base_currency") val isBaseCurrency: Boolean
)