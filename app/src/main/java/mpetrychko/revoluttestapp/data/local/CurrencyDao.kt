package mpetrychko.revoluttestapp.data.local

import androidx.room.*

@Dao
abstract class CurrencyDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insert(vararg currencyEntity: CurrencyEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertAll(currencyEntity: List<CurrencyEntity>)

    @Query("select * from currency where parent=:baseCurrencyName")
    abstract fun loadRelativeCurrency(baseCurrencyName: String): List<CurrencyEntity>?

    @Query("select * from currency where is_base_currency=1")
    abstract fun findBaseCurrency(): CurrencyEntity?

    @Query("delete from currency where is_base_currency=1")
    abstract fun deleteBaseCurrency()

    @Transaction
    open fun saveBaseCurrency(currencyEntity: CurrencyEntity) {
        deleteBaseCurrency()
        insert(currencyEntity)
    }
}