package mpetrychko.revoluttestapp

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import mpetrychko.revoluttestapp.domain.Currency
import mpetrychko.revoluttestapp.domain.UpdateRatesUseCase
import mpetrychko.revoluttestapp.util.Event
import mpetrychko.revoluttestapp.util.postEvent
import timber.log.Timber

class RatesViewModel(
    updateRatesUseCase: UpdateRatesUseCase
) : ViewModel() {

    private val _currencyState: MutableLiveData<UpdateRatesUseCase.RatesState> = MutableLiveData()
    val currencyState: LiveData<UpdateRatesUseCase.RatesState> = _currencyState

    private val _showNoDataAvailableAtTheMoment: MutableLiveData<Event<Unit>> = MutableLiveData()
    val showNoDataAvailableAtTheMoment: LiveData<Event<Unit>> = _showNoDataAvailableAtTheMoment

    private val baseCurrencyBehaviorSubject =
        BehaviorSubject.create<UpdateRatesUseCase.RatesAction>()

    private val compositeDisposable = CompositeDisposable()

    init {
        compositeDisposable.add(updateRatesUseCase
            .updateRates(baseCurrencyBehaviorSubject)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ currentState ->
                _currencyState.value = currentState
                if (currentState.showNoDataAvailableAtTheMoment && currentState.relativeCurrency != null) {
                    _showNoDataAvailableAtTheMoment.postEvent(Unit)
                }
            }) { // should never happen
                _currencyState.value = UpdateRatesUseCase.RatesState(showUnknownError = true)
                Timber.e(it)
            })
    }

    fun onStartTrackingRate() {
        baseCurrencyBehaviorSubject.onNext(UpdateRatesUseCase.StartTrackingRateAction)
    }

    fun onStopTrackingRate() {
        baseCurrencyBehaviorSubject.onNext(UpdateRatesUseCase.StopTrackingRateAction)
    }

    fun onBaseCurrencyChanged(baseCurrency: Currency) {
        baseCurrencyBehaviorSubject.onNext(
            UpdateRatesUseCase.ChangeCurrentBaseCurrencyAction(
                baseCurrency
            )
        )
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

    class RatesViewModelFactory(
        private val updateRatesUseCase: UpdateRatesUseCase
    ) :
        ViewModelProvider.NewInstanceFactory() {
        @Suppress("UNCHECKED_CAST")
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return RatesViewModel(updateRatesUseCase) as T
        }
    }
}