package mpetrychko.revoluttestapp.ext

import java.text.NumberFormat
import java.text.ParseException

fun NumberFormat.formatNullable(number: Number?): String? =
    if (number == null) null else {
        try {
            format(number)
        } catch (ignored: IllegalArgumentException) { // should never happen
            null
        }
    }

fun NumberFormat.parseNullable(s: String?): Double? =
    s?.let {
        if (s.isBlank()) null else {
            try {
                parse(s)?.toDouble()
            } catch (ignored: ParseException) {
                null
            }
        }
    }