package mpetrychko.revoluttestapp

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import androidx.recyclerview.widget.*
import mpetrychko.revoluttestapp.databinding.ActivityRatesBinding
import mpetrychko.revoluttestapp.databinding.ItemCurrencyBinding
import mpetrychko.revoluttestapp.di.Injection
import mpetrychko.revoluttestapp.domain.Currency
import mpetrychko.revoluttestapp.ext.formatNullable
import mpetrychko.revoluttestapp.ext.parseNullable
import mpetrychko.revoluttestapp.util.EventObserver
import timber.log.Timber
import java.text.NumberFormat

class RatesActivity : AppCompatActivity() {

    private val ratesViewModel: RatesViewModel by viewModels {
        RatesViewModel.RatesViewModelFactory(
            Injection.provideCurrencyRateUseCase(this)
        )
    }

    private val currencyAdapter by lazy {
        CurrencyAdapter(
            layoutInflater,
            numberFormat,
            { ratesViewModel.onBaseCurrencyChanged(it) },
            { ratesViewModel.onBaseCurrencyChanged(it) },
            inputManager
        )
    }

    private val numberFormat = NumberFormat.getNumberInstance().apply {
        maximumFractionDigits = 2
        isGroupingUsed = false
    }

    private val inputManager by lazy { getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager }

    private lateinit var binding: ActivityRatesBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRatesBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.toolbar.setNavigationOnClickListener { finish() }

        binding.rvCurrencyRates.adapter = currencyAdapter
        currencyAdapter.stateRestorationPolicy =
            RecyclerView.Adapter.StateRestorationPolicy.PREVENT_WHEN_EMPTY
        binding.rvCurrencyRates.itemAnimator =
            DefaultItemAnimator().apply { supportsChangeAnimations = false }
        currencyAdapter.registerAdapterDataObserver(object : RecyclerView.AdapterDataObserver() {
            override fun onItemRangeMoved(fromPosition: Int, toPosition: Int, itemCount: Int) {
                super.onItemRangeMoved(fromPosition, toPosition, itemCount)
                Timber.d(
                    "moved from %s tom %s with %s",
                    fromPosition,
                    toPosition,
                    itemCount
                )
                if (toPosition == 0) {
                    (binding.rvCurrencyRates.layoutManager as? LinearLayoutManager)?.scrollToPositionWithOffset(
                        toPosition,
                        0
                    )
                }
            }
        })

        ratesViewModel.currencyState.observe(this, { currencyRates ->
            when {
                currencyRates.isLoading && currencyRates.relativeCurrency == null -> {
                    binding.pbLoading.show()
                    binding.tvError.isVisible = false
                    binding.flOfflineMode.isInvisible = true
                }
                currencyRates.showNetworkError && currencyRates.relativeCurrency == null -> {
                    binding.pbLoading.hide()
                    binding.tvError.setCompoundDrawablesWithIntrinsicBounds(
                        0,
                        R.drawable.ic_baseline_wifi_off_48,
                        0,
                        0
                    )
                    binding.tvError.text = getString(R.string.no_internet_connection)
                    binding.tvError.isVisible = true
                    binding.flOfflineMode.isInvisible = true
                    binding.rvCurrencyRates.isVisible = false
                }
                currencyRates.showUnknownError && currencyRates.relativeCurrency == null -> {
                    binding.pbLoading.hide()
                    binding.tvError.setCompoundDrawablesWithIntrinsicBounds(
                        0,
                        R.drawable.ic_baseline_error_48,
                        0,
                        0
                    )
                    binding.tvError.text = getString(R.string.something_went_wrong)
                    binding.tvError.isVisible = true
                    binding.flOfflineMode.isInvisible = true
                    binding.rvCurrencyRates.isVisible = false
                }
                currencyRates.relativeCurrency != null -> {
                    binding.rvCurrencyRates.isVisible = true
                    binding.tvError.isVisible = false
                    binding.flOfflineMode.isInvisible = !currencyRates.showIsWorkingInOfflineMode
                    binding.pbLoading.hide()
                    currencyAdapter.submitList(arrayListOf(currencyRates.relativeCurrency.baseCurrency) + currencyRates.relativeCurrency.targetCurrencies)
                }
            }
        })
        ratesViewModel.showNoDataAvailableAtTheMoment.observe(this, EventObserver {
            Toast.makeText(
                this,
                getString(R.string.no_data_available),
                Toast.LENGTH_LONG
            )
                .show()
        })
    }

    override fun onStart() {
        super.onStart()
        ratesViewModel.onStartTrackingRate()
    }

    override fun onStop() {
        super.onStop()
        ratesViewModel.onStopTrackingRate()
    }

    private inner class CurrencyAdapter(
        private val layoutInflater: LayoutInflater,
        private val numberFormat: NumberFormat,
        private val onBaseCurrencyRateChanged: (Currency) -> Unit,
        private val onNewCurrencySet: (Currency) -> Unit,
        private val inputManager: InputMethodManager
    ) :
        ListAdapter<Currency, CurrencyViewHolder>(CURRENCY_ITEM_CALLBACK) {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CurrencyViewHolder {
            return CurrencyViewHolder(
                ItemCurrencyBinding.inflate(layoutInflater, parent, false),
                numberFormat,
                onBaseCurrencyRateChanged,
                onNewCurrencySet,
                inputManager
            )
        }

        override fun onBindViewHolder(holder: CurrencyViewHolder, position: Int) {
            holder.bindTo(getItem(position))
        }

        override fun onBindViewHolder(
            holder: CurrencyViewHolder,
            position: Int,
            payloads: MutableList<Any>
        ) {
            if (payloads.isNotEmpty() && payloads.first() is CurrencyRateWasChangedPayload) {
                holder.bindWhenBaseCurrencyRateWasChanged(getItem(position))
            } else {
                super.onBindViewHolder(holder, position, payloads)
            }
        }
    }

    private class CurrencyViewHolder(
        val binding: ItemCurrencyBinding,
        private val numberFormat: NumberFormat,
        private val onBaseCurrencyRateChanged: (Currency) -> Unit,
        private val onNewCurrencySet: (Currency) -> Unit,
        private val inputManager: InputMethodManager
    ) :
        RecyclerView.ViewHolder(binding.root) {

        private lateinit var currency: Currency

        private val rateTextWatcher = object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                // do nothing
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                // do nothing
            }

            override fun afterTextChanged(s: Editable?) {
                onBaseCurrencyRateChanged(
                    currency.copy(
                        currencyRate = numberFormat.parseNullable(s.toString())
                    )
                )
            }
        }

        private val onChangeRateClickListener = View.OnClickListener {
            onNewCurrencySet(currency)
            binding.etCurrencyRate.requestFocus()
            binding.etCurrencyRate.setSelection(
                binding.etCurrencyRate.text?.toString()?.length ?: 0
            )
            inputManager.showSoftInput(binding.etCurrencyRate, InputMethodManager.SHOW_IMPLICIT)
        }

        private val rateFocusListener = View.OnFocusChangeListener { _, isInFocus ->
            if (isInFocus) {
                onNewCurrencySet(currency)
            } else {
                inputManager.hideSoftInputFromWindow(binding.etCurrencyRate.windowToken, 0)
            }
        }

        private val onEditorListener =
            TextView.OnEditorActionListener { _, actionId, _ ->
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    binding.etCurrencyRate.clearFocus()
                }
                false
            }

        fun bindTo(currency: Currency) {
            this.currency = currency
            binding.tvCurrency.text = currency.currencyName
            binding.tvCurrencyName.text = currency.currencyName
            binding.etCurrencyRate.removeTextChangedListener(rateTextWatcher)
            val formattedRate = numberFormat.formatNullable(currency.currencyRate).orEmpty()
            if (currency.isBaseCurrency) {
                if (!binding.etCurrencyRate.hasFocus()) { // do not change the base currency
                    binding.etCurrencyRate.setText(formattedRate)
                }
                binding.etCurrencyRate.addTextChangedListener(rateTextWatcher)
                itemView.setOnClickListener(null)
            } else {
                binding.etCurrencyRate.setText(formattedRate)
                itemView.setOnClickListener(onChangeRateClickListener)
            }
            binding.etCurrencyRate.onFocusChangeListener = rateFocusListener
            binding.etCurrencyRate.setOnEditorActionListener(onEditorListener)
        }

        fun bindWhenBaseCurrencyRateWasChanged(currency: Currency) {
            this.currency = currency
            val formattedRate = numberFormat.formatNullable(currency.currencyRate).orEmpty()
            if (!binding.etCurrencyRate.hasFocus()) { // do not change the base currency
                binding.etCurrencyRate.setText(formattedRate)
            }
        }
    }

    sealed class CurrencyPayload

    object CurrencyRateWasChangedPayload : CurrencyPayload()

    companion object {
        private val CURRENCY_ITEM_CALLBACK = object : DiffUtil.ItemCallback<Currency>() {
            override fun areItemsTheSame(oldItem: Currency, newItem: Currency): Boolean {
                return oldItem.currencyName == newItem.currencyName
            }

            override fun areContentsTheSame(oldItem: Currency, newItem: Currency): Boolean {
                return oldItem == newItem
            }

            override fun getChangePayload(oldItem: Currency, newItem: Currency): Any? {
                return if (oldItem.currencyName == newItem.currencyName && oldItem.currencyRate != newItem.currencyRate
                    && oldItem.isBaseCurrency == newItem.isBaseCurrency
                ) CurrencyRateWasChangedPayload else null
            }
        }
    }
}
