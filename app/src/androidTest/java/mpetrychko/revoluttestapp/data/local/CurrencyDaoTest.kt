package mpetrychko.revoluttestapp.data.local

import androidx.room.Room
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.google.common.truth.Truth.assertThat
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException

@RunWith(AndroidJUnit4::class)
class CurrencyDaoTest {

    private lateinit var currencyDao: CurrencyDao
    private lateinit var currencyDatabase: CurrencyDatabase

    private val testBaseCurrency = CurrencyEntity("c5", "p5", 5.0, true)
    private val newTestBaseCurrency =
        CurrencyEntity("c6", "p6", 6.0, true)

    private val testCurrencies = arrayListOf(
        CurrencyEntity("c1", "p1", 1.0, false),
        CurrencyEntity("c2", "p2", 2.0, false),
        CurrencyEntity("c3", "p3", 3.0, false),
        CurrencyEntity("c4", "p4", 4.0, false),
        testBaseCurrency
    )

    @Before
    fun init() {
        val context = InstrumentationRegistry.getInstrumentation().context
        currencyDatabase = Room.inMemoryDatabaseBuilder(
            context, CurrencyDatabase::class.java
        ).build()
        currencyDao = currencyDatabase.currencyDao()
    }

    @After
    @Throws(IOException::class)
    fun tearDown() {
        currencyDatabase.close()
    }

    @Test
    fun whenInsertCurrency_verifyItsThere() {
        val testCurrency = testCurrencies[0]
        currencyDao.insert(testCurrency)
        val currencyEntities = currencyDao.loadRelativeCurrency(testCurrency.parent)
        assertThat(currencyEntities).isNotNull()
        assertThat(currencyEntities?.size).isEqualTo(1)
        val insertedEntity = currencyEntities?.first()
        assertThat(insertedEntity).isEqualTo(testCurrency)
    }

    @Test
    fun whenInsertCurrencyVarArgs_verifyItsThere() {
        val testCurrency1 = testCurrencies[0]
        val testCurrency2 = testCurrencies[1]
        currencyDao.insert(testCurrency1, testCurrency2)
        val currencyEntities = readCurrenciesWithParents(arrayListOf(testCurrency1, testCurrency2))
        assertThat(currencyEntities).isNotNull()
        assertThat(currencyEntities.size).isEqualTo(2)
        assertThat(currencyEntities.contains(testCurrency1)).isTrue()
        assertThat(currencyEntities.contains(testCurrency2)).isTrue()
    }

    @Test
    fun whenInsertCurrencies_verifyItsThere() {
        currencyDao.insertAll(testCurrencies)
        readCurrenciesWithParentsAndVerifyEquality()
    }

    private fun readCurrenciesWithParentsAndVerifyEquality() {
        val currencyEntities = readCurrenciesWithParents()
        assertThat(currencyEntities).isNotNull()
        assertThat(currencyEntities.size).isEqualTo(testCurrencies.size)
        assertThat(currencyEntities.all {
            testCurrencies.contains(it)
        }).isTrue()
    }

    private fun readCurrenciesWithParents(targetEntitiesWithParents: List<CurrencyEntity> = testCurrencies): List<CurrencyEntity> {
        return targetEntitiesWithParents.map { it.parent }.toHashSet()
            .flatMap { currencyDao.loadRelativeCurrency(it).orEmpty() }
    }

    @Test
    fun whenInsertDuplicates_replaceThem() {
        currencyDao.insertAll(testCurrencies)
        currencyDao.insertAll(testCurrencies)
        readCurrenciesWithParentsAndVerifyEquality()
    }

    @Test
    fun whenFindBaseCurrency_verifyItIsFound() {
        currencyDao.insertAll(testCurrencies)
        val baseCurrency = currencyDao.findBaseCurrency()
        assertThat(baseCurrency).isNotNull()
        assertThat(baseCurrency).isEqualTo(testBaseCurrency)
    }

    @Test
    fun whenDeleteBaseCurrency_verifyItIsDeleted() {
        currencyDao.insertAll(testCurrencies)
        currencyDao.deleteBaseCurrency()
        val currencyEntities = readCurrenciesWithParents()
        assertThat(currencyEntities.size).isEqualTo(testCurrencies.size - 1)
        assertThat(currencyEntities.contains(testBaseCurrency)).isFalse()
    }

    @Test
    fun saveBaseCurrency_verifyItIsSaved() {
        currencyDao.insertAll(testCurrencies)
        currencyDao.saveBaseCurrency(newTestBaseCurrency)
        val baseCurrency = currencyDao.findBaseCurrency()
        assertThat(baseCurrency).isEqualTo(newTestBaseCurrency)
    }

    @Test
    fun saveBaseCurrency_verifyPreviousBaseCurrencyIsRemoved() {
        currencyDao.insertAll(testCurrencies)
        currencyDao.saveBaseCurrency(newTestBaseCurrency)
        val currencyEntities = readCurrenciesWithParents()
        assertThat(currencyEntities).isNotEmpty()
        assertThat(currencyEntities.contains(testBaseCurrency)).isFalse()
    }

}